c**********************************************************************
c     Copyright (c) 2020 NTNU PhysMet
c
c     Permission is hereby granted, free of charge, to any person 
c     obtaining a copy of this software and associated documentation 
c     files (the "Software"), to deal in the Software without 
c     restriction, including without limitation the rights to use, 
c     copy, modify, merge, publish, distribute, sublicense, and/or 
c     sell copies of the Software, and to permit persons to whom the 
c     Software is furnished to do so, subject to the following 
c     conditions:
c
c     The above copyright notice and this permission notice shall be 
c     included in all copies or substantial portions of the Software.
c
c     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
c     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
c     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
c     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
c     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
c     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
c     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
c     OTHER DEALINGS IN THE SOFTWARE.
c
c     Contributors: Tomas Manik (tomas.manik@ntnu.no)
c                   Bjorn Holmedal (bjorn.holmedal@ntnu.no)
c
c     Reference as "Manik, T., 2021. A natural vector/matrix notation 
c     applied in an efficient and robust return-mapping algorithm for 
c     advanced yield functions, Eur J Mech a-Solid, Volume 90,
c     doi.org/10.1016/j.euromechsol.2021.104357"
c
c
c
      subroutine umat(stress, statev, ddsdde, sse, spd, scd,
     1 rpl, ddsddt, drplde, drpldt,
     2 stran, dstran, time, dtime, temp, dtemp, predef, dpred, cmname,
     3 ndi, nshr, ntens, nstatv, props, nprops, coords, drot, pnewdt,
     4 celent, dfgrd0, dfgrd1, noel, npt, layer, kspt, jstep, kinc)
c
      include 'aba_param.inc'

      character*80 cmname
      dimension stress(ntens), statev(nstatv),
     1 ddsdde(ntens, ntens), ddsddt(ntens), drplde(ntens),
     2 stran(ntens), dstran(ntens), time(2), predef(1), dpred(1),
     3 props(nprops), coords(3), drot(3,3), dfgrd0(3,3), dfgrd1(3,3),
     4 jstep(4)
c
c     user coding to define ddsdde, stress, statev, sse, spd, scd
c     and, if necessary, rpl, ddsddt, drplde, drpldt, pnewdt
c
c**********************************************************************
c     This is a user-defined material subroutine UMAT that: 
c      - can be used to define the mechanical constitutive behavior of 
c        a material.
c      - will be called at all material calculation points of elements 
c        for which the material definition includes a user-defined 
c        material behavior
c      - can be used with any procedure that includes mechanical 
c        behavior
c      - can use solution-dependent state variables
c      - must update the stresses and solution-dependent state variables
c        to their values at the end of the increment for which it is 
c        called
c      - must provide the material Jacobian matrix DDSDDE for the 
c        mechanical constitutive model
c      - can be used in conjunction with user subroutine USDFLD to 
c        redefine any field variables before they are passed in.  
c      
c     This implementation of UMAT includes:
c      - isotropic elasticity
c      - isotropic Voce hardening law
c      - YLD2004 yield surface developed by Barlat, F. et al. 
c        Int. J. Plast. 2005, doi:10.1016/j.ijplas.2004.06.004
c      - implicit backward-Euler return map algorithm with radial return
c        initial guess and line-search based on Scherzinger, W. M.:  
c        A return mapping algorithm for isotropic and anisotropic plasticity 
c        models using a line search method, Computer Methods in Applied
c        Mechanics and Engineering, 2017, doi: 10.1016/j.cma.2016.11.026
c      - natural vector and matrix notation for 2nd and 4th order tensors
c      - can be referenced as "Manik, T., 2021. A natural vector/matrix  
c        notation applied in an efficient and robust return-mapping algorithm  
c        for advanced yield functions, Eur J Mech a-Solid, Volume 90,
c        doi.org/10.1016/j.euromechsol.2021.104357"
c
c**********************************************************************
c (OUT) DDSDDE is an array, dimension (NTENS,NTENS)
c     Jacobian matrix of the constitutive model. DDSDDE(I,J) defines the 
c     change in the Ith stress component at the end of the time 
c     increment caused by an infinitesimal perturbation of the Jth 
c     component of the strain increment array. Unless you invoke the 
c     unsymmetric equation solution capability for the user-defined 
c     material, ABAQUS/Standard will use only the symmetric part of 
c     DDSDDE. The symmetric part of the matrix is calculated by taking 
c     one half the sum of the matrix and its transpose. For viscoelastic 
c     behavior in the frequency domain, the Jacobian matrix must be 
c     dimensioned as DDSDDE(NTENS,NTENS,2). The stiffness contribution 
c     (storage modulus) must be provided in DDSDDE(NTENS,NTENS,1), 
c     while the damping contribution (loss modulus) must be provided in 
c     DDSDDE(NTENS,NTENS,2). 
c
c (IN/OUT) STRESS ia an array, dimension (NTENS)
c     This array is passed in as the stress tensor at the beginning
c     of the increment and must be updated in this routine to be the
c     stress tensor at the end of the increment. If you specified
c     initial stresses, this array will contain the initial 
c     stresses at the start of the
c     analysis. The size of this array depends on the value of NTENS
c     as defined below. In finite-strain problems the stress tensor
c     has already been rotated to account for rigid body motion in
c     the increment before UMAT is called, so that only the
c     corotational part of the stress integration should be done in 
c     UMAT. The measure of stress used is “true” (Cauchy) stress. 
c     If the UMAT utilizes a hybrid formulation that is total (as 
c     opposed to the default incremental behavior), the stress array 
c     is extended beyond NTENS. Check the Abaqus Documentation for 
c     the details. For storing of the components of a symmetric
c     stress tensor as a 1-dimensional array, Abaqus applies the 
c     Voigt convention ordered as (Sxx, Syy, Szz, Sxy, Syz, Sxz), 
c     i.e. Sxx, Syy and Szz are the normal stress components, 
c     Sxy, Syz and Sxz are the shear components. 
c
c (IN/OUT) STATEV is an array, dimension (NSTATV)
c     An array containing the solution-dependent state variables.
c     These are passed in as the values at the beginning of the
c     increment unless they are updated in user subroutines USDFLD
c     or UEXPAN, in which case the updated values are passed in. In all 
c     cases STATEV must be returned as the values at the end of the 
c     increment. The size of the array is defined as described in
c     “Allocating space” in the Abaqus Documentation. In finite-strain 
c     problems any vector-valued or tensor-valued state variables must 
c     be rotated to account for rigid body motion of the material, 
c     in addition to any update in the values associated with 
c     constitutive behavior. The rotation increment matrix, DROT, is 
c     provided for this purpose.
c
c (IN/OUT) SSE, SPD, SCD
c     Specific elastic strain energy, plastic dissipation, and
c     “creep” dissipation, respectively. These are passed in as
c     the values at the start of the increment and should be
c     updated to the corresponding specific energy values at
c     the end of the increment. They have no effect on the solution,
c     except that they are used for energy output.
c
c *** Only in a fully coupled thermal-stress analysis ***
c (OUT) RPL
c     Volumetric heat generation per unit time at the end of the 
c     increment caused by mechanical working of the material.
c
c (OUT) DDSDDT is an array, dimension (NTENS)
c     Variation of the stress increments with respect to the temperature.
c
c (OUT) DRPLDE is an array, dimension (NTENS)
c     Variation of RPL with respect to the strain increments.
c
c (OUT) DRPLDT
c     Variation of RPL with respect to the temperature.
c
c *** Variables that can be updated ***
c (OUT) "optional" PNEWDT
c     Ratio of suggested new time increment to the time increment being
c     used (DTIME, see discussion later in this section). This variable
c     allows you to provide input to the automatic time incrementation
c     algorithms in ABAQUS/Standard (if automatic time incrementation is
c     chosen). For a quasi-static procedure the automatic time stepping 
c     that ABAQUS/Standard uses, which is based on techniques for 
c     integrating standard creep laws, cannot be controlled from within
c     the UMAT subroutine. PNEWDT is set to a large value before each 
c     call to UMAT. If PNEWDT is redefined to be less than 1.0, 
c     ABAQUS/Standard must abandon the time increment and attempt it 
c     again with a smaller time increment. The suggested new time 
c     increment provided to the automatic time integration algorithms is
c     PNEWDT × DTIME, where the PNEWDT used is the minimum value
c     for all calls to user subroutines that allow redefinition of 
c     PNEWDT for this iteration. If PNEWDT is given a value that is 
c     greater than 1.0 for all calls to user subroutines for this 
c     iteration and the increment converges in this iteration, 
c     ABAQUS/Standard may increase the time increment. The suggested new
c     time increment provided to the automatic time integration 
c     algorithms is PNEWDT × DTIME, where the PNEWDT used is the minimum
c     value for all calls to user subroutines for this iteration.
c     If automatic time incrementation is not selected in the analysis 
c     procedure, values of PNEWDT that are greater than 1.0 will be 
c     ignored and values of PNEWDT that are less than 1.0 will cause the
c     job to terminate.
c
c *** Variables passed in for information ***
c (IN) STRAN is an array, dimension (NTENS)
c     An array containing the total strains at the beginning of the 
c     increment. If thermal expansion is included in the same material 
c     definition, the strains passed into UMAT are the mechanical 
c     strains only (that is, the thermal strains computed based upon 
c     the thermal expansion coefficient have been subtracted from the 
c     total strains). These strains are available for output
c     as the “elastic” strains. In finite-strain problems the strain 
c     components have been rotated to account for rigid body motion in 
c     the increment before UMAT is called and are approximations to 
c     logarithmic strain. For storing of the components of a symmetric
c     strain tensor as a 1-dimensional array, Abaqus applies the 
c     Voigt convention ordered as (Exx, Eyy, Ezz, 2*Exy, 2*Eyz, 2*Exz), 
c     i.e. Exx, Eyy and Ezz are the normal strain components, 
c     Exy, Eyz and Exz are the shear components. Note, that Abaqus 
c     always reports shear strain as engineering shear strain.
c
c (IN) DSTRAN is an array, dimension (NTENS)
c     Array of strain increments. If thermal expansion is included in 
c     the same material definition, these are the mechanical strain 
c     increments (the tota) strain increments minus the thermal strain 
c     increments). For storing of the components of a symmetric
c     strain increment tensor as a 1-dimensional array, Abaqus applies 
c     the Voigt convention ordered as 
c     (dExx, dEyy, dEzz, 2*dExy, 2*dEyz, 2*dExz), 
c     i.e. dExx, dEyy and dEzz are the normal strain increment 
c     components, dExy, dEyz and dExz are the shear increment 
c     components. Note, that Abaqus always reports shear strain 
c     increments as engineering shear strain increments.
c
c (IN) TIME(1)
c     Value of step time at the beginning of the current increment.
c
c (IN) TIME(2)
c     Value of total time at the beginning of the current increment.
c
c (IN) DTIME
c     Time increment.
c
c (IN) TEMP
c     Temperature at the start of the increment.
c
c (IN) DTEMP
c     Increment of temperature.
c
c (IN) PREDEF
c     Array of interpolated values of predefined field variables at this
c     point at the start of the increment, based on the values read in 
c     at the nodes.
c
c (IN) DPRED
c     Array of increments of predefined field variables.
c
c (IN) CMNAME
c     User-defined material name, left justified. Some internal material 
c     models are given names starting with the “ABQ_” character string. 
c     To avoid conflict, you should not use “ABQ_” as the leading string 
c     for CMNAME.
c
c (IN) NDI
c     Number of direct stress components at this point.
c
c (IN) NSHR
c     Number of engineering shear stress components at this point.
c
c (IN) NTENS
c     Size of the stress or strain component array (NDI + NSHR).
c
c (IN) NSTATV
c     Number of solution-dependent state variables that are associated 
c     with this material type (defined as described in “Allocating space” 
c     in Abaqus Documentation.
c
c (IN) PROPS is an array, dimension (NPROPS)
c     User-specified array of material constants associated with this 
c     user material.
c
c (IN) NPROPS
c     User-defined number of material constants associated with this 
c     user material.
c
c (IN) COORDS
c     An array containing the coordinates of this point. These are the 
c     current coordinates if geometric nonlinearity is accounted for 
c     during the step, otherwise, the array contains the original 
c     coordinates of the point.
c
c (IN) DROT is an array, dimensions (3,3)
c     Rotation increment matrix. This matrix represents the increment of
c     rigid body rotation of the basis system in which the components of 
c     stress (STRESS) and strain (STRAN) are stored. It is provided so 
c     that vector- or tensor-valued state variables can be rotated 
c     appropriately in this subroutine: stress and strain components are
c     already rotated by this amount before UMAT is called. This matrix 
c     is passed in as a unit matrix for small-displacement analysis and 
c     for large-displacement analysis if the basis system for the 
c     material point rotates with the material (as in a shell element or
c     when a local orientation is used).
c
c (IN) CELENT
c     Characteristic element length, which is a typical length of a line
c     across an element for a first-order element; it is half of the same
c     typical length for a second-order element. For beams and trusses 
c     it is a characteristic length along the element axis. For membranes
c     and shells it is a characteristic length in the reference surface. 
c     For axisymmetric elements it is a characteristic length in the  
c     plane only. For cohesive elements it is equal to the constitutive
c     thickness.
c
c (IN) DFGRD0 is an array, dimension (3,3)
c     Array containing the deformation gradient at the beginning of the 
c     increment. See the discussion regarding the availability of the 
c     deformation gradient for various element types.
c
c (IN) DFGRD1 is an array, dimension (3,3)
c     Array containing the deformation gradient at the end of the 
c     increment. The components of this array are set to zero if 
c     nonlinear geometric effects are not included in the step 
c     definition associated with this increment. See the discussion 
c     regarding the availability of the deformation gradient for
c     various element types.
c
c (IN) NOEL
c     Element number.
c
c (IN) NPT
c     Integration point number.
c
c (IN) LAYER
c     Layer number (for composite shells and layered solids).
c
c (IN) KSPT
c     Section point number within the current layer.
c
c (IN) KSTEP
c     Step number.
c
c (IN) KINC
c     Increment number.
c
      integer     maxiter, info
      real*8      tol, svec(6), dstrain(6), am(6,6)
      logical     doprint, firstinc
      real*8      ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
      real*8      factor
      common /consts/ ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
      common /fact/ factor
      data   firstinc /.TRUE./
c
c ------- SOLUTION DEPENDENT STATE VARIABLES USED IN THIS UMAT --------
c
c     STATEV(1) - the equivalent plastic strain EP
c     STATEV(2) - the equivalent plastic strain rate DEP/DTIME
c     STATEV(3) - the isoropic hardening stress R
c     STATEV(4) - number of Newton-Raphson iterations to converge
c     STATEV(5) - the largest number of line-searches within one iter.
c     STATEV(6) - return-map convergence status (see INFO in returnmap)
c     STATEV(7) - the converged value of the residual PSI
c
c --------------- MATERIAL PARAMETERS USED IN THIS UMAT ---------------
c 
c     PROPS(1)  - Young's modulus
c     PROPS(2)  - Poisson's ratio
c     PROPS(3)  - reserved for future yield function identificator (= 0)
c     PROPS(4)  - initial yield stress
c     PROPS(5)  - saturation stress Rsat and 
c     PROPS(6)  - saturation strain esat in Voce hardening law 
c                 (see below)
c     PROPS(7)  - exponent of the yield function Yld2004
c     PROPS(8)  - \   18 anisotropy coef
c       ...     - |>  of the yield function Yld2004
c     PROPS(25) - /   defined in the natural notation, see relations below
c
c     In Yld2004-18p, the anisotropy L-coefficients 
c     in PROPS(8:25) are related to the C-coefficients as follows:
c
c     L12 = 1/(3*sqrt(2))*( C12 - 2*C13 + C21 - 2*C23 +   C31 +   C32)
c     L13 =     1/sqrt(6)*(-C12         + C21         +   C31 -   C32)
c     L22 =           1/6*(-C12 + 2*C13 - C21 + 2*C23 + 2*C31 + 2*C32)
c     L23 = 1/(2*sqrt(3))*( C12         - C21         + 2*C31 - 2*C32)
c     L32 = 1/(2*sqrt(3))*(-C12 + 2*C13 + C21 - 2*C23)
c     L33 =           1/2*( C12         + C21)
c     L44 = C44
c     L55 = C55
c     L66 = C66
c
c     where C-coefficients make up the C matrix in the Voigt notation as
c
c                  |  0  -C12 -C13   0    0    0 |         
c                  |-C21   0  -C23   0    0    0 |
c                  |-C31 -C32   0    0    0    0 |
c	             |  0    0    0   C44   0    0 |
c                  |  0    0    0    0   C55   0 |
c                  |  0    0    0    0    0   C66|     
c
c     and the L-coefficients make up the L matrix in the natural notation as
c
c                  |  0   L12  L13   0    0    0 |         
c                  |  0   L22  L23   0    0    0 |
c                  |  0   L32  L33   0    0    0 |
c	             |  0    0    0   L44   0    0 |
c                  |  0    0    0    0   L55   0 |
c                  |  0    0    0    0    0   L66|     
c  
c     The Voce isotropic hardening law is implemented as
c
c                     R(ep) = Rsat*(1-exp(-ep/esat))
c     and                sy = s0 + R(ep) 
c                     
c     where ep   is the equivalent plastic strain,
c           sy   is the flow stress
c           s0   is the initial flow stress
c           R    is the isotropic hardening stress
c           Rsat is the saturation stress,
c           esat is the saturation strain
c
c ---------------------- DEFINING SOME CONSTANTS ----------------------
c 
      ov2     = 0.5d0
	ov3     = 1.d0/3.d0
      ovsqrt3 = 1.d0/sqrt(3.d0)
      ovsqrt2 = 1.d0/sqrt(2.d0)
      ovsqrt6 = ovsqrt3*ovsqrt2
	sqrt2   = 1.d0/ovsqrt2
      sqrt3   = 1.d0/ovsqrt3
c     defining tolerance for the return-map algorithm
      tol = 1.d-18
c     defining maximum number of tierations for the return-map algorithm
      maxiter = 100												  
c     disable controlled printing of the iteration process
      doprint = .FALSE.
c     stress initial guess factor 
      factor = 1.0d0
c
c     print the material parameters in .dat file
      if (firstinc) then
          write(6,200) (PROPS(i),i=1,NPROPS), tol, factor
          firstinc = .FALSE.
      end if
c
c ----- STRESS CONVERTION FROM ABAQUS-VOIGT TO NATURAL NOTATION -----
c 
      svec(1) = ovsqrt3*(STRESS(1)+STRESS(2)+STRESS(3))
	svec(2) = ovsqrt6*(2.d0*STRESS(3)-STRESS(1)-STRESS(2))
	svec(3) = ovsqrt2*(STRESS(2)-STRESS(1))
	svec(4) = sqrt2*STRESS(6)
	svec(5) = sqrt2*STRESS(5)
	svec(6) = sqrt2*STRESS(4)
c
c ----- STRAIN CONVERTION FROM ABAQUS-VOIGT TO NATRUAL NOTATION -----
c 
      dstrain(1) = ovsqrt3*(DSTRAN(1)+DSTRAN(2)+DSTRAN(3))
	dstrain(2) = ovsqrt6*(2.d0*DSTRAN(3)-DSTRAN(1)-DSTRAN(2))
	dstrain(3) = ovsqrt2*(DSTRAN(2)-DSTRAN(1))
	dstrain(4) = ovsqrt2*DSTRAN(6)
	dstrain(5) = ovsqrt2*DSTRAN(5)
	dstrain(6) = ovsqrt2*DSTRAN(4)
c
c ------------ THE CORE ROUTINE - RETURN-MAPPING ALGORITHM ------------
c
      call returnmap(svec, dstrain, DTIME, PROPS, NPROPS, STATEV,  
     +               NSTATV, am, tol, info, doprint)
c
c ------ STRESS CONVERTION FROM NATURAL TO ABAQUS-VOIGT NOTATION -------
c   
      STRESS(1) = ovsqrt3*svec(1) - ovsqrt6*svec(2) - ovsqrt2*svec(3)
      STRESS(2) = ovsqrt3*svec(1) - ovsqrt6*svec(2) + ovsqrt2*svec(3)
      STRESS(3) = ovsqrt3*svec(1) + 2.d0*ovsqrt6*svec(2)
      STRESS(4) = ovsqrt2*svec(6)
      STRESS(5) = ovsqrt2*svec(5)
      STRESS(6) = ovsqrt2*svec(4)
c
c ---- ALG. MODULUS CONVERTION FROM NATURAL TO ABAQUS-VOIGT NOTATION ----
c 
      DDSDDE(1,1) = ov3*(am(1,1) + ov2*am(2,2) + sqrt3*am(2,3)
     +            + 3.d0/2.d0*am(3,3))
      DDSDDE(1,2) = ov3*(am(1,1) + ov2*am(2,2) - 3.d0/2.d0*am(3,3))
      DDSDDE(1,3) = ov3*(am(1,1) - am(2,2) - sqrt3*am(2,3))
      DDSDDE(1,4) = -ov2*(ovsqrt3*am(2,6) + am(3,6))
      DDSDDE(1,5) = -ov2*(ovsqrt3*am(2,5) + am(3,5))
      DDSDDE(1,6) = -ov2*(ovsqrt3*am(2,4) + am(3,4))
      DDSDDE(2,2) = ov3*(am(1,1) + ov2*am(2,2) - sqrt3*am(2,3)
     +            + 3.d0/2.d0*am(3,3))
      DDSDDE(2,3) = ov3*(am(1,1) - am(2,2) + sqrt3*am(2,3))
      DDSDDE(2,4) = -ov2*(ovsqrt3*am(2,6) - am(3,6))
      DDSDDE(2,5) = -ov2*(ovsqrt3*am(2,5) - am(3,5))
      DDSDDE(2,6) = -ov2*(ovsqrt3*am(2,4) - am(3,4))
      DDSDDE(3,3) = ov3*(am(1,1) + 2.d0*am(2,2))
      DDSDDE(3,4) = ovsqrt3*am(2,6)
      DDSDDE(3,5) = ovsqrt3*am(2,5)
      DDSDDE(3,6) = ovsqrt3*am(2,4)
      DDSDDE(4,4) = ov2*am(6,6)
      DDSDDE(4,5) = ov2*am(5,6)
      DDSDDE(4,6) = ov2*am(4,6)
      DDSDDE(5,5) = ov2*am(5,5)
      DDSDDE(5,6) = ov2*am(4,5)
      DDSDDE(6,6) = ov2*am(4,4)
c     and symmetric lower triangle
      do i=1, 6
          do j=i+1, 6
              DDSDDE(j,i) = DDSDDE(i,j)
          end do
      end do
c
      return
c     http://patorjk.com/software/taag/#p=display&f=Big&t=UMAT
200   format(/
     +        5x, '***********************************'/,
     +        5x, '*   _    _ __  __       _______   *'/,
     +        5x, '*  | |  | |  \/  |   /\|__   __|  *'/,
     +        5x, '*  | |  | | \  / |  /  \  | |     *'/,
     +        5x, '*  | |  | | |\/| | / /\ \ | |     *'/,
     +        5x, '*  | |__| | |  | |/ ____ \| |     *'/,
     +        5x, '*   \____/|_|  |_/_/    \_\_|     *'/,
     +        5x, '*                                 *'/,
     +        5x, '***********************************'/,
     +        5x, '     in the natural notation       '/,
     +        5x, '                                   '/,
     +        5x, '              WITH                 '/,
     +        5x, '      MATERIAL PARAMETERS:         '/,
     +        5x, '                                   '/,
     +        5x, ' Elasticity                        '/,
     +        5x, ' E        = ',               1pe12.5/,
     +        5x, ' nu       = ',               1pe12.5/,
     +        5x, ' Yield function type               '/,
     +        5x, ' yldftype = ',               1pe12.5/,
     +        5x, ' Plasticity and hardening          '/, 
     +        5x, ' s0       = ',               1pe12.5/,
     +        5x, ' Rsat     = ',               1pe12.5/,
     +        5x, ' esat     = ',               1pe12.5/,
     +        5x, ' Yield function Yld2004-18p        '/,
     +        5x, ' a        = ',               1pe12.5/,
     +        5x, ' L1-12    = ',               1pe12.5/,
     +        5x, ' L1-13    = ',               1pe12.5/,
     +        5x, ' L1-22    = ',               1pe12.5/, 
     +        5x, ' L1-23    = ',               1pe12.5/, 
     +        5x, ' L1-32    = ',               1pe12.5/, 
     +        5x, ' L1-33    = ',               1pe12.5/, 
     +        5x, ' L1-44    = ',               1pe12.5/, 
     +        5x, ' L1-55    = ',               1pe12.5/, 
     +        5x, ' L1-66    = ',               1pe12.5/, 
     +        5x, ' L2-12    = ',               1pe12.5/,
     +        5x, ' L2-13    = ',               1pe12.5/,
     +        5x, ' L2-22    = ',               1pe12.5/,
     +        5x, ' L2-23    = ',               1pe12.5/,
     +        5x, ' L2-32    = ',               1pe12.5/,
     +        5x, ' L2-33    = ',               1pe12.5/,
     +        5x, ' L2-44    = ',               1pe12.5/,
     +        5x, ' L2-55    = ',               1pe12.5/,
     +        5x, ' L2-66    = ',               1pe12.5/,
     +        5x, ' Return-map parameters             '/, 
     +        5x, ' tol      = ',               1pe12.5/,
     +        5x, ' factor   = ',               1pe12.5/)
      end subroutine umat
c
c     
      subroutine returnmap(stress, dstrain, dt, params, np, sdv, ns,
     +                     algmod, tol, INFO, doprint)
c**********************************************************************
c      This is the return mapping predictor-corrector algorithm for 
c      solving the closest-point projection in an elastic-plastic 
c      problem. It is based on a Newton-Raphson method with 
c      line-search. This algorithm is based on the paper by 
c      Scherzinger, W. M.: A return mapping algorithm for isotropic 
c      and anisotropic plasticity models using
c      a line search method, Computer Methods in Applied Mechanics
c      and Engineering, 2017, DOI: 10.1016/j.cma.2016.11.026
c      This particular implementation is for isotropic elasticity, 
c      isotropic Voce hardening and Yld2004-18p yield surface.
c
c      Reference as "Manik, T., 2021. A natural vector/matrix notation 
c      applied in an efficient and robust return-mapping algorithm for 
c      advanced yield functions, Eur J Mech a-Solid, Volume 90,
c      doi.org/10.1016/j.euromechsol.2021.104357"
c
c**********************************************************************
c (IN/OUT) STRESS is REAL*8 array, dimension (6)
c          It is Cauchy stress at the beginning of the time 
c          increment expressed in the natural notation. At return it is 
c          updated by the subroutine to be the stress at the end of the
c          time increment.
c (IN)     DSTRAIN is REAL*8 array, dimension (6)
c          It is the total strain increment expressed in the natural
c          notation.
c (IN)     DT is REAL*8
c          It is a time increment
c (IN)     PARAMS is REAL*8 array, dimension (NP)
c          It contains user-defined necessary material parameters 
c          as e.g. elastic constants, plastic anisotropy coefficients...
c (IN)     NP is INTEGER
c          It defines the length of the PARAMS array.
c (IN)     SDV is REAL*8 array, dimension (NS)
c          It contains the solution-dependent state variables as e.g.
c          equivalent plastic strain, hardening model variables,
c          algorithm convergence info
c (IN)     NS is INTEGER
c          It defines the length of the SDV array.
c (OUT)    ALGMOD is REAL*8 array, dimension (6,6)
c          It is the algorithmic modulus defined as dstress/dstrain.
c (IN)     TOL is REAL*8
c          At input, it is the tolerance so the algorithm terminates 
c          when the residual psi < tol, where psi is given by Eq. 44.
c          At return, it contains the last value of residual psi. 
c (OUT)    INFO is INTEGER
c          INFO = 0 means that the algorithm finished successfully with
c          use of the Newton-Raphson steps only, i.e. no need for 
c          line-seach
c          INFO = 1 means that the algorithm finished successfully and
c          the line-search was activated at least once
c          INFO = 2 means that the algorithm finished successfully 
c          within IMAX iterations but there was at least one 
c          iteration in which line-search was terminated due to 
c          reaching JMAX line-search iterations
c          INFO = 3 means that there was no need for plastic corrector
c          since the yield function at STRIAL <= 0.d0
c          INFO = 4 means that there was no need for plastic corrector
c          since the initial return-map guess solved the residuals
c          INFO = -1 means unsuccesfull finish, i.e. the residual PSI
c          did not get below TOL within IMAX iterations
c (IN)     DOPRINT is LOGICAL
c          DOPRINT = .TRUE., enables controlled printing of the 
c          iteration process
c          DOPRINT = .FALSE., any info printing is suppressed 
      implicit none  
c     ! in/out
      integer     info, np, ns
      real*8      stress(6), dstrain(6), dt, tol, params(np), 
     +            sdv(ns), algmod(6,6)
      logical     doprint
c     ! internal
      integer     i, j, k, iter, imax, jmax, yldftype, jlargest
      real*8      Dp(5), DpI(5), res(5), strial(5), f, grad(5), 
     +            hess(5,5), alpha, psi0, psi, lamdot, Linv(5,5),
     +            shearmod2, bulkmod3, a, L(5,5), beta,
     +            dlamdot, eta, dsdev(5), lamdotI, sdevI(5), p(5),
     +            sy, vdot, y(5), y2(5), aparams(18), sdev(5), s0,
     +            esat, ep, epI, R, Rsat, Rovesat, ovesat, depI, 
     +            H, RI, choldiag(5), denom, Emod, nu, hard
      logical     linesearch
      real*8      ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
      real*8      factor
      common /consts/ ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
      common /fact/ factor
c     Newton-Raphson and line-search parameters
      beta = 1.d-4
      eta  = 0.1d0
      jmax = 10
      imax = 200
      linesearch = .false.
      jlargest = 0
c
c --- PARAMETERS READING ------- 
c
c     Young modulus
      Emod = params(1)
c     Poisson's ratio
      nu   = params(2)
c     double of the shear modulus
      shearmod2 = Emod/(1.d0+nu)
c     triple of the bulk modulus
      bulkmod3  = Emod/(1.d0-2.d0*nu)
c     type of the yield function (to be implemented)
      yldftype = int(params(3))
c     initial yield stress
      s0 = params(4)
c     isotropic hardening, here Voce law implemented 
c     as R(ep) = Rsat*(1-exp(-ep/esat)) and
c     yield stress is sy = s0 + R(ep)
      Rsat = params(5)
      esat = params(6)
      Rovesat = Rsat/esat
      ovesat = 1.d0/esat
c     Yld2004-18p - exponent and plastic anisotropy coef for Yld2004
      a = params(7)
      do i=1, 18
          aparams(i) = params(i+7)
      end do
c
c --- SOLUTION-DEPENDENT STATE VARIABLES FROM THE LAST STEP -------
c     
c     equivalent plastic strain
      ep = sdv(1)
c     equivalent plastic strain rate (no need to read)
c     sdv(2)
c     isotropic hardening
      R = sdv(3)
c     number of iterations used (no need to read)
c     sdv(4)
c     max number of line-searches for one Newton iter
c     sdv(5)
c     convergence INFO
c     sdv(6)
c     final value of PSI residual (no need to read)
c     sdv(7)
c
c     yield stress
      sy = s0 + R
c     isotorpic hardening modulus
      hard = ovesat*(Rsat-R)
c
c --- ELASTIC PREDICTOR -------
c
c     hydrostatic pressure update
      stress(1) = stress(1) + bulkmod3*dstrain(1)
c
      do i=1, 5
c         compute deviatoric trial stress
          strial(i) = stress(i+1) + shearmod2*dstrain(i+1)
      end do
c
c     yield function at STRIAL
      call Yld2004(strial, aparams, a, 0, f, grad, hess)
c
c     if no need for the plastic corrector
      if ((f-sy) .LT. 0.d0) then
c         update the stress
          do i=1, 5 
              stress(i+1) = strial(i)
          end do
c         caluclate the algorithmic modulus   
          algmod(1,1) = bulkmod3
          algmod(2,2) = shearmod2
          algmod(3,3) = shearmod2
          algmod(4,4) = shearmod2
          algmod(5,5) = shearmod2
          algmod(6,6) = shearmod2
          algmod(1:3,4:6) = 0.d0
          algmod(1,2) = 0.d0
          algmod(1,3) = 0.d0
          algmod(2,3) = 0.d0
          algmod(4,5) = 0.d0
          algmod(4,6) = 0.d0
          algmod(5,6) = 0.d0
c         solution flag
          INFO = 3
c         update the state variables after pure elastic step
c         sdv(1) = no need to update
          sdv(2) = 0.d0
c         sdv(3) = no need to update
          sdv(4) = 0.d0
          sdv(5) = 0.d0
          sdv(6) = real(INFO)
          sdv(7) = 0.d0
          goto 101
      end if
c
c --- PLASTIC CORRECTOR -------
c
c     making the initial guess
      do i=1, 5
c         radial return of trial stress
          sdev(i) = strial(i)*sy/f*factor
c         initialize plastic strain rate
          Dp(i) = -(sdev(i)-strial(i))/shearmod2/dt
      end do
c     initial guess for plastic multiplier
      lamdot = vdot(Dp, sdev, 5)/sy
c     initial guess for the hardening stress R
      R = Rsat*(1.d0-exp(-(ep + lamdot*dt)*ovesat))
      sy = s0 + R
c     compute residuals
      call Yld2004(sdev, aparams, a, 1, f, grad, hess)
c
      do i=1, 5
          res(i) = -Dp(i) + lamdot*grad(i)
      end do
c     calculate the merit function
      psi0 = ov2*(vdot(res,res,5)*dt**2 + ((f-sy)/shearmod2)**2)
c
      if (doprint) write(*, *) 'Psi initial ', psi0
c
c     case when initial radial return guess solves the residuals
      if (psi0 .LT. tol) then
          psi = psi0
          INFO = 4
          iter = 0
          goto 102
      end if
c
c     start the iterations
      do iter=1, imax
c
          if (doprint) write(*, *) 'Iter no. ', iter
c
c         start with the Newton step, i.e. alpha = 1
          alpha = 1.d0
c         calculate yield func, its gradient and hessian
          call Yld2004(sdev, aparams, a, 2, f, grad, hess)
c         due to major symmetry of both elastic modulus and
c         the Hessian, only the upper triangle is calculated
          do i=1, 5
              do k=i, 5
                  Linv(i,k) = lamdot*hess(i,k)
                  if (i .EQ. k) then
                      Linv(i,k) = Linv(i,k) + 1.d0/shearmod2/dt
                  end if
              end do
          end do    
c         solve Linv*y = grad by Cholesky decomposition, i.e.
c         y = L*grad
c         (lower triangle of Linv will be modified but the upper
c          triangle incl. the diagonal will not)
          call chol_decomp(Linv, 5, choldiag)
          call chol_solve(Linv, 5, grad, choldiag, y)
c         plastic modulus H
          H = ovesat*(Rsat-R)*dt
c         yield stress
          sy = s0 + R
c         calculate plastic multiplier increment
          denom = vdot(grad,y,5) + H
          dlamdot = ((f-sy) - vdot(res,y,5))/denom
c         
          do i=1, 5
              y2(i) = res(i) + dlamdot*grad(i)
          end do
c         solve Linv*dsdev = y2 by Cholesky decomposition
          call chol_solve(Linv, 5, y2, choldiag, dsdev)
c
          do i=1, 5
              dsdev(i) = -dsdev(i)
              sdevI(i) = sdev(i) + dsdev(i)
c             update plastic strain rate
              DpI(i) = -(sdevI(i)-strial(i))/shearmod2/dt
          end do  
c         update plastic multiplier
          lamdotI = lamdot + dlamdot
          depI = lamdotI*dt
c         update the yield stress (Voce law)
          RI = Rsat*(1.d0-exp(-(ep + depI)*ovesat))
          sy = s0 + RI
c         yield function
          call Yld2004(sdevI, aparams, a, 1, f, grad, hess)
c         calculate residuals
          do i=1, 5
              res(i) = -DpI(i) + lamdotI*grad(i)
          end do
          psi = ov2*(vdot(res,res,5)*dt**2 + ((f-sy)/shearmod2)**2)
c         Newton-Raphson step end
c         
          if (doprint) write(*, *) 'Psi after Newton step ', psi
c
c         start line-search, if not good enough improvement
c         by the Newton-Raphson step
          j = 0  
          do while ((psi .GT. (1.d0-2.d0*beta*alpha)*psi0) 
     +               .and. (j .LE. jmax))
c             flag indicating activated line-search
              linesearch = .TRUE.
              j = j + 1
c             find new alpha as
              alpha = max(eta*alpha, (psi0*alpha**2)
     +              / (psi-(1.d0-2.d0*alpha)*psi0))
c
              if (doprint) write(*, *) 'Alpha ', alpha
c
c             update deviatoric stress
              do i=1, 5
                  sdevI(i) = sdev(i) + alpha*dsdev(i)
              end do
c             update plastic multiplier
              lamdotI = lamdot + alpha*dlamdot
              depI = lamdotI*dt
c             update plastic strain rate
              do i=1, 5
                  DpI(i) = -(sdevI(i)-strial(i))/shearmod2/dt
              end do
c             update the yield stress (Voce law)
              RI = Rsat*(1.d0-exp(-(ep + depI)*ovesat))
              sy = s0 + RI
c             yield function
              call Yld2004(sdevI, aparams, a, 1, f, grad, hess)
c             residuals
              do i=1, 5
                  res(i) = -DpI(i) + lamdotI*grad(i)
              end do
              psi = ov2*(vdot(res,res,5)*dt**2 
     +            + ((f - sy)/shearmod2)**2)
c
              if (doprint) write(*, *) 'Psi in Linesearch ', psi
c
          end do
c         save max number of line-searches
          if (j .GT. jlargest) jlargest = j
c         make candidate solution being the solution of this iteration
          do i=1, 5
              sdev(i) = sdevI(i)
              Dp(i) = DpI(i)
          end do
          lamdot = lamdotI
          psi0   = psi
          R = RI
          if (j .GT. jmax) INFO = 2
c         leave the loop if converged
          if (psi0 .LT. tol) exit
      end do
c
c     assign INFO
      if (psi .LT. tol) then
          if (linesearch) then
c             succesful return-map by use of line-search and
c             without reaching JMAX line-search iterations
              if (INFO .NE. 2) INFO = 1
          else
c             succesful return-map by pure Newton-Raphson steps
              INFO = 0
          end if
      else
c         not success
          INFO = -1
      end if
c
102   continue
c     continue from goto of the case when radial-return guess solved psi
c
c --- STRESS UPDATE (DEVIATORIC PART) -------
c
      do i=1,5
          stress(i+1) = sdev(i)
      end do
c
c --- SOLUTION-DEPENDENT VARIABLES UPDATE -------
c
c     equivalent plastic strain update
      sdv(1) = ep + lamdot*dt
      sdv(2) = lamdot
c     isotropic hardening
      sdv(3) = R
c     number of iterations used
      sdv(4) = real(iter)
c     largest number of line-searches
      sdv(5) = real(jlargest)
c     INFO flag
      sdv(6) = real(INFO)
c     final value of PSI residual
      sdv(7) = psi
c
c --- ALGORITHMIC MODULUS UPDATE -------
c     (due to major symmetry only upper triangle is calculated)
      call Yld2004(sdev, aparams, a, 2, f, grad, hess)
c
      do i=1, 5
          do k=i, 5
              Linv(i,k) = lamdot*hess(i,k)
              if (i .EQ. k) then
                  Linv(i,k) = Linv(i,k) + 1.d0/shearmod2/dt
              end if
          end do
      end do    
      call chol_decomp(Linv, 5, choldiag)
      call chol_inverse(Linv, 5, choldiag, L) 
      do i=1, 5
          y(i) = 0.d0
          do j=1, 5
              y(i) = y(i) + L(i,j)*grad(j)
          end do
      end do
      H = ovesat*(Rsat-R)*dt
      denom = vdot(grad,y,5) + H
c
      algmod(1,1) = bulkmod3
      do j=2, 6
          algmod(1,j) = 0.d0
      end do
      do i=2, 6
          do j=i, 6
              algmod(i,j) = (L(i-1,j-1) - y(i-1)*y(j-1)/denom)/dt
          end do
      end do
c
101   return
      end subroutine returnmap
c
c
      subroutine eig(svec, mode, e1, e2, e3, evec1, evec2, evec3)
c**********************************************************************
c      Calculates eigenvalues and eigenvectors of a 3x3 symmetrix 
c      matrix written in the natural notation as 6x1 vector. Based on 
c      paper by Scherzinger, W. M. and Dohrmann, C. R., A robust  
c      algorithm for finding the eigenvalues and eigenvectors of 3x3 
c      symmetric matrices, Computer Methods in Applied Mechanics and 
c      Engineering, 2008, DOI: 10.1016/j.cma.2008.03.031
c
c**********************************************************************
c (IN)    SVEC is REAL*8 array, dimension (3,3)
c         It is a 6x1 vector representing a symmetric 3x3 tensor in 
c         the natural notation
c (IN)    MODE is integer
c         MODE = 0 returns only eigenvalues, eigenvectors will not be 
c         calculated
c         MODE = else returns both eigenvalues and eigenvectors
c (OUT)   E1, E2, E3 are REAL*8 
c         three eigenvalues
c (OUT)   EVEC1, EVEC2, EVEC3 are REAL*8 array, dimension (3)
c         three eigenvectors
c         
c**********************************************************************
      implicit none  
c     ! in/out
      integer     mode
      real*8      svec(6), e1, e2, e3, evec1(3), evec2(3), evec3(3)
c     ! internal
      integer     i, j, flag, imax, k, m
      real*8      J2, J3, PI, r(3), nr, sdott, Amat(3,3), u1(3), u2(3),
     +            t(3,2), As1(3), As2(3), AR11, AR22, AR23, AR32,   
     +            AR33, s1(3), s2(3), a1, a2, a3, cos3a, ns, w1(3)
      real*8      ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
      parameter (PI = 3.14159265359d0)
      common /consts/ ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
c
c     build 3x3 deviatoric matrix from svec given in the natural notation
      Amat(1,1) = - ovsqrt6*svec(2) - ovsqrt2*svec(3)
      Amat(2,2) = - ovsqrt6*svec(2) + ovsqrt2*svec(3)
      Amat(3,3) = + 2.d0*ovsqrt6*svec(2)
      Amat(2,3) = ovsqrt2*svec(4)
      Amat(1,3) = ovsqrt2*svec(5)
      Amat(1,2) = ovsqrt2*svec(6)
      Amat(3,2) = Amat(2,3)
      Amat(3,1) = Amat(1,3)
      Amat(2,1) = Amat(1,2)
c
      J2 = 0.d0
      do i=2, 6
          J2 = J2 + svec(i)**2
      end do
      J2 = ov2*J2
c
      J3 = Amat(1,1)*(Amat(2,2)*Amat(3,3)-Amat(3,2)**2)
     +   + Amat(1,3)*(2.d0*Amat(2,1)*Amat(3,2) - Amat(2,2)*Amat(1,3))
     +   - Amat(3,3)*Amat(2,1)**2
c     
      if (J2 .LT. 1.d-30) then
          e1 = 0.d0
          e2 = 0.d0
          e3 = 0.d0
          evec1(1) = 1.d0
          evec1(2) = 0.d0
          evec1(3) = 0.d0
          evec2(1) = 0.d0
          evec2(2) = 1.d0
          evec2(3) = 0.d0
          evec3(1) = 0.d0
          evec3(2) = 0.d0
          evec3(3) = 1.d0
      goto 100
      end if
      cos3a = ov2*J3*(3.d0/J2)**(3.d0/2.d0)
c     to make cos3a within [-1, 1] interval
      cos3a = max(-1.d0, min(1.d0, cos3a))
c
      a1 = ov3*acos(cos3a)
      a3 = a1 + 2.d0*ov3*PI
      a2 = a1 + 4.d0*ov3*PI
c
      if (a1 .LT. PI/6.d0) then
          e1 = 2.d0*sqrt(J2*ov3)*cos(a1)
      else
          e1 = 2.d0*sqrt(J2*ov3)*cos(a3)
      end if
c      
      do i=1, 3
          Amat(i,i) = Amat(i,i) - e1
      end do
c
c     Find the largest column of Amat and store as s1
      ns = 0.d0
      do j=1, 3
          nr = Amat(1,j)**2+Amat(2,j)**2+Amat(3,j)**2
          if (nr .GT. ns) then
              ns = nr
              imax = j
              do i=1, 3
                  s1(i) = Amat(i,j)
              end do
          end if
      end do
c
      do i=1, 3
          s1(i) = s1(i)/sqrt(ns)
      end do
c
      m = 1
      do j=1, 3
          if (j .NE. imax) then
              sdott = s1(1)*Amat(1,j)+s1(2)*Amat(2,j)+s1(3)*Amat(3,j)
              do i=1, 3
                  t(i,m) = Amat(i,j) - sdott*s1(i)
              end do
              m = m+1
          end if
      end do
c
c     Find the largest t column and store as s2
      ns = 0.d0
      do j=1, 2
          nr = t(1,j)**2+t(2,j)**2+t(3,j)**2
          if (nr .GT. ns) then
              ns = nr
              do i=1, 3
                  s2(i) = t(i,j)
              end do
          end if
      end do
c
      do i=1, 3
          s2(i) = s2(i)/sqrt(ns)
      end do
c
c     First eigenvector v1
      evec1(1) = s1(2)*s2(3)-s2(2)*s1(3)
      evec1(2) = s1(3)*s2(1)-s2(3)*s1(1)
      evec1(3) = s1(1)*s2(2)-s2(1)*s1(2)
c
c     Build reduced form of A' matrix (Eq. 22)
      do i=1, 3
          Amat(i,i) = Amat(i,i) + e1
      end do
c
      AR11 = e1
      do i=1, 3
          As1(i) = Amat(i,1)*s1(1) + Amat(i,2)*s1(2) + Amat(i,3)*s1(3)
          As2(i) = Amat(i,1)*s2(1) + Amat(i,2)*s2(2) + Amat(i,3)*s2(3)
      end do
c
      AR22 = s1(1)*As1(1) + s1(2)*As1(2) + s1(3)*As1(3)
      AR23 = s1(1)*As2(1) + s1(2)*As2(2) + s1(3)*As2(3)
      AR32 = s2(1)*As1(1) + s2(2)*As1(2) + s2(3)*As1(3)
      AR33 = s2(1)*As2(1) + s2(2)*As2(2) + s2(3)*As2(3)
c
c     Find the remaining eigenvalues e2, e3 by the Wilkinsons shift
      e2 = ov2*(AR22+AR33) - ov2*sign(1.d0, AR22-AR33)
     +   * sqrt((AR22-AR33)**2 + 4.d0*AR23*AR32)
      e3 = AR22 + AR33 - e2
c
c     returns here if only eigenvalues are required
      if (mode .EQ. 0) goto 100
c
c     Find eigenvectors evec2 and evec3
      do i=1, 3
          Amat(i,i) = Amat(i,i) - e2
      end do
c
      do i=1, 3
          u1(i) = Amat(i,1)*s1(1) + Amat(i,2)*s1(2) + Amat(i,3)*s1(3)
          u2(i) = Amat(i,1)*s2(1) + Amat(i,2)*s2(2) + Amat(i,3)*s2(3)
      end do
c
      nr = u1(1)**2 + u1(2)**2 + u1(3)**2
      ns = u2(1)**2 + u2(2)**2 + u2(3)**2
c     if s1 and s2 are already second and third eigenvectors, then
c     both u1 and u2 and their norms equal zero (ELSE branch)
      if ((nr .GT. 1.d-30) .or. (ns .GT. 1.d-30)) then
          if (nr .GT. ns) then
              do i=1, 3
                  w1(i) = u1(i)/sqrt(nr)
              end do
          else
              do i=1, 3
                  w1(i) = u2(i)/sqrt(ns)
              end do
          end if
          evec2(1) = w1(2)*evec1(3)-evec1(2)*w1(3)
          evec2(2) = w1(3)*evec1(1)-evec1(3)*w1(1)
          evec2(3) = w1(1)*evec1(2)-evec1(1)*w1(2)
c
          evec3(1) = evec1(2)*evec2(3)-evec2(2)*evec1(3)
          evec3(2) = evec1(3)*evec2(1)-evec2(3)*evec1(1)
          evec3(3) = evec1(1)*evec2(2)-evec2(1)*evec1(2)
      else
          do i=1, 3
              evec2(i) = s1(i)
              evec3(i) = s2(i)
          end do
      end if
c
c     adding pressure to get final eigenvalues
100   e1 = e1 + ovsqrt3*svec(1)
      e2 = e2 + ovsqrt3*svec(1)
      e3 = e3 + ovsqrt3*svec(1)
c      
      return
      end subroutine eig
c
c     
      subroutine Yld2004(svec, cparams, a, mode, f, grad, hessian)
c**********************************************************************
c      Calculates Barlat's Yld2004 yield function and/or its gradient
c      and/or its Hessian expressed in the natural notation. Algorithm  
c      is based on the paper by Scherzinger, W. M.: A return mapping 
c      algorithm for isotropic and anisotropic plasticity models using
c      a line search method, Computer Methods in Applied Mechanics
c      and Engineering, 2017, DOI: 10.1016/j.cma.2016.11.026
c
c**********************************************************************
c (IN)    SVEC is REAL*8 array, dimension (5)
c         It is stress tensor given in the natural notation
c (IN)    CPARAMS is REAL*8 array, dimension (:)
c         It is 18 anisotropic parameters of Yld2004 expected in the
c         natural notation, i.e. the first L anisotropic transformation 
c         matrix read
c
c         | 0  L12 L13  0   0   0 |         
c         | 0  L22 L23  0   0   0 |
c         | 0  L32 L33  0   0   0 |
c         | 0   0   0  L44  0   0 |
c         | 0   0   0   0  L55  0 |
c         | 0   0   0   0   0  L66|
c
c         Coefficients in LPARAMS are ordered as
c         L'12, L'13, L'22, L'23, L'32, L'33, L'44, L'55, L'66, 
c         followed by coeffiients associated with the second anistropic 
c         transformation matrix L'' as
c         L''12, L''13, L''22, L''23, L''32, L''33, L''44, L''55, L''66
c (IN)    A is REAL*8
c         It is the exponent in Yld2004 (a must be >= 2)
c (IN)    MODE is INTEGER
c         Controls which of F, GRAD, HESSIAN is to be calculated
c         = 0  :  only F is calculated
c         = 1  :  only F and GRAD is calculated
c         else :  F, GRAD and HESSIAN are calculated
c (OUT)   F is REAL*8
c         It is scalar value of Yld2004 yield function
c (OUT)   GRAD is REAL*8 array, dimension (5)
c         It is gradient of Yld2004 yield function, computed in 
c         the natural notation
c (OUT)   HESSIAN is REAL*8 array, dimension (5,5)
c         It is the Hessian of Yld2004 yield function computed in 
c         the natural notation
c         
c**********************************************************************
      implicit none
c     ! in/out
      integer     mode
      real*8      svec(5), cparams(18), a, f, grad(5), hessian(5,5)
c     ! internal
      integer     i, j, emod
      real*8      s1vec(6), s2vec(6), scale, tol, asms(9), tmp(6),      
     +            tmpnew(6), tmp1, tmp2, tmp3
c
      real*8      L12, L13, L22, L23, L32, L33, L44, L55, L66,          
     +            M12, M13, M22, M23, M32, M33, M44, M55, M66,          
     +            s11, s12, s13, s21, s22, s23,                         
     +            eigvec11(3), eigvec12(3), eigvec13(3),                
     +            eigvec21(3), eigvec22(3), eigvec23(3),                
     +            s11s21, s11s22, s11s23,                               
     +            s12s21, s12s22, s12s23,                               
     +            s13s21, s13s22, s13s23,                               
     +            as11s21pa1, as11s22pa1, as11s23pa1,                   
     +            as12s21pa1, as12s22pa1, as12s23pa1,                   
     +            as13s21pa1, as13s22pa1, as13s23pa1,
     +            as11s21pa2, as11s22pa2, as11s23pa2,                   
     +            as12s21pa2, as12s22pa2, as12s23pa2,                   
     +            as13s21pa2, as13s22pa2, as13s23pa2,  	 
     +            dfds11, dfds12, dfds13, dfds21, dfds22, dfds23,       
     +            ddfds11ds11, ddfds12ds12, ddfds13ds13,                
     +            ddfds21ds21, ddfds22ds22, ddfds23ds23,                
     +            ddfds11ds12, ddfds11ds13,                             
     +            ddfds12ds11, ddfds12ds13,                             
     +            ddfds13ds11, ddfds13ds12,                             
     +            ddfds21ds22, ddfds21ds23,                             
     +            ddfds22ds21, ddfds22ds23,                             
     +            ddfds23ds21, ddfds23ds22,                             
     +            ddfds11ds21, ddfds12ds22, ddfds13ds23,                
     +            ddfds11ds22, ddfds11ds23,                             
     +            ddfds12ds21, ddfds12ds23,                             
     +            ddfds13ds21, ddfds13ds22,                             
     +            ddfds21ds11, ddfds21ds12,                             
     +            ddfds21ds13, ddfds22ds11,                             
     +            ddfds22ds12, ddfds22ds13,                             
     +            ddfds23ds11, ddfds23ds12, ddfds23ds13,                
     +            e11xe11v(6), e11xe12v(6), e11xe13v(6),                
     +            e12xe11v(6), e12xe12v(6), e12xe13v(6),                
     +            e13xe11v(6), e13xe12v(6), e13xe13v(6),                
     +            e21xe21v(6), e21xe22v(6), e21xe23v(6),                
     +            e22xe21v(6), e22xe22v(6), e22xe23v(6),                
     +            e23xe21v(6), e23xe22v(6), e23xe23v(6),                
     +            ds11xds11(6,6), ds11xds12(6,6), ds11xds13(6,6),       
     +            ds12xds11(6,6), ds12xds12(6,6), ds12xds13(6,6),       
     +            ds13xds11(6,6), ds13xds12(6,6), ds13xds13(6,6),       
     +            ds21xds21(6,6), ds21xds22(6,6), ds21xds23(6,6),       
     +            ds22xds21(6,6), ds22xds22(6,6), ds22xds23(6,6),       
     +            ds23xds21(6,6), ds23xds22(6,6), ds23xds23(6,6),       
     +            ds11xds21(6,6), ds11xds22(6,6), ds11xds23(6,6),       
     +            ds12xds21(6,6), ds12xds22(6,6), ds12xds23(6,6),       
     +            ds13xds21(6,6), ds13xds22(6,6), ds13xds23(6,6),       
     +            ds21xds11(6,6), ds21xds12(6,6), ds21xds13(6,6),       
     +            ds22xds11(6,6), ds22xds12(6,6), ds22xds13(6,6),       
     +            ds23xds11(6,6), ds23xds12(6,6), ds23xds13(6,6),       
     +            Etmp1212(6,6), Etmp1221(6,6), Etmp2112(6,6),          
     +            Etmp2121(6,6), Etmp2323(6,6), Etmp2332(6,6),          
     +            Etmp3223(6,6), Etmp3232(6,6), Etmp3131(6,6),          
     +            Etmp3113(6,6), Etmp1331(6,6), Etmp1313(6,6),          
     +            E1212p(6,6), E2323p(6,6), E3131p(6,6),                
     +            E1212pp(6,6), E2323pp(6,6), E3131pp(6,6),             
     +            coefE1212p, coefE2323p, coefE3131p,                   
     +            coefE1212pp, coefE2323pp, coefE3131pp,                
     +            H1(6,6), H2(6,6), H3(6,6), H4(6,6), H5(6,6), H6(6,6), 
     +            H7(6,6), H8(6,6), LTxH7xL(5,5), MTxH8xM(5,5), 
     +            dsymLTxH3xM(5,5)             
c
      real*8      ov3, ov4, ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, 
     +            mov4, a2, a1, a1ovf, ova, ov2
      common /consts/ ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
c
      ov4     = 0.25d0
      mov4    = -ov4
      a2      = a-2.d0
      a1      = a-1.d0 
      ova     = 1.d0/a
      tol     = 1.d-16
c      
      L12  = cparams(1)
      L13  = cparams(2)
      L22  = cparams(3)
      L23  = cparams(4)
      L32  = cparams(5)
      L33  = cparams(6)
      L44  = cparams(7)
      L55  = cparams(8)
      L66  = cparams(9)
      M12  = cparams(10)
      M13  = cparams(11)
      M22  = cparams(12)
      M23  = cparams(13)
      M32  = cparams(14)
      M33  = cparams(15)
      M44  = cparams(16)
      M55  = cparams(17)
      M66  = cparams(18)
c
      s1vec(1) = L12*svec(1) + L13*svec(2)
      s1vec(2) = L22*svec(1) + L23*svec(2)
      s1vec(3) = L32*svec(1) + L33*svec(2)
      s1vec(4) = L44*svec(3)
      s1vec(5) = L55*svec(4)
      s1vec(6) = L66*svec(5)
c
      s2vec(1) = M12*svec(1) + M13*svec(2)
      s2vec(2) = M22*svec(1) + M23*svec(2)
      s2vec(3) = M32*svec(1) + M33*svec(2)
      s2vec(4) = M44*svec(3)
      s2vec(5) = M55*svec(4)
      s2vec(6) = M66*svec(5)
c
      if (mode .EQ. 0) then
          emod = 0
      else
          emod = 1
      end if
c     Calculate eigenvalues and eigenvectors of transformed stresses
      call eig(s1vec, emod, s11, s12, s13, eigvec11, eigvec12, eigvec13)
      call eig(s2vec, emod, s21, s22, s23, eigvec21, eigvec22, eigvec23)
c      
c     
      asms(1) = abs(s11-s21)
      asms(2) = abs(s11-s22)
      asms(3) = abs(s11-s23)
c              
      asms(4) = abs(s12-s21)
      asms(5) = abs(s12-s22)
      asms(6) = abs(s12-s23)
c              
      asms(7) = abs(s13-s21)
      asms(8) = abs(s13-s22)
      asms(9) = abs(s13-s23)
c
      scale = asms(1)
      do i=2, 9
          if (asms(i) .GT. scale) scale = asms(i)
      enddo
c     
      f = 0.d0
      if (scale .GT. tol) then
          do i=1, 9
              f = f + (asms(i)/scale)**a
          enddo
c
c         Compute yield function F
          f = scale*(ov4*f)**ova
      end if
c
      if (mode .EQ. 0) return    
c
      a1ovf   = a1/f
c     rescaling eigenstresses by F
      s11 = s11/f
      s12 = s12/f
      s13 = s13/f
c
      s21 = s21/f
      s22 = s22/f
      s23 = s23/f
c
      s11s21 = s11-s21
      s11s22 = s11-s22
      s11s23 = s11-s23
c      
      s12s21 = s12-s21
      s12s22 = s12-s22
      s12s23 = s12-s23
c
      s13s21 = s13-s21
      s13s22 = s13-s22
      s13s23 = s13-s23
c
      as11s21pa1 = abs(s11s21)**a1
      as11s22pa1 = abs(s11s22)**a1
      as11s23pa1 = abs(s11s23)**a1
c      
      as12s21pa1 = abs(s12s21)**a1
      as12s22pa1 = abs(s12s22)**a1
      as12s23pa1 = abs(s12s23)**a1
c      
      as13s21pa1 = abs(s13s21)**a1
      as13s22pa1 = abs(s13s22)**a1
      as13s23pa1 = abs(s13s23)**a1
c
cc
      dfds11 = ov4*( sign(1.d0,s11s21)*as11s21pa1 
     +             + sign(1.d0,s11s22)*as11s22pa1            
     +             + sign(1.d0,s11s23)*as11s23pa1)
      dfds12 = ov4*( sign(1.d0,s12s21)*as12s21pa1 
     +             + sign(1.d0,s12s22)*as12s22pa1            
     +             + sign(1.d0,s12s23)*as12s23pa1)                                   
      dfds13 = ov4*( sign(1.d0,s13s21)*as13s21pa1 
     +             + sign(1.d0,s13s22)*as13s22pa1            
     +             + sign(1.d0,s13s23)*as13s23pa1)                                   
cc                                                                    
      dfds21 = ov4*(- sign(1.d0,s11s21)*as11s21pa1 
     +              - sign(1.d0,s12s21)*as12s21pa1            
     +              - sign(1.d0,s13s21)*as13s21pa1)                                   
      dfds22 = ov4*(- sign(1.d0,s11s22)*as11s22pa1 
     +              - sign(1.d0,s12s22)*as12s22pa1            
     +              - sign(1.d0,s13s22)*as13s22pa1)                                   
      dfds23 = ov4*(- sign(1.d0,s11s23)*as11s23pa1 
     +              - sign(1.d0,s12s23)*as12s23pa1            
     +              - sign(1.d0,s13s23)*as13s23pa1)
c
      tmp(1) = dfds11*eigvec11(1)*eigvec11(1)
     +       + dfds12*eigvec12(1)*eigvec12(1)
     +       + dfds13*eigvec13(1)*eigvec13(1)
      tmp(2) = dfds11*eigvec11(2)*eigvec11(2)
     +       + dfds12*eigvec12(2)*eigvec12(2)
     +       + dfds13*eigvec13(2)*eigvec13(2)
      tmp(3) = dfds11*eigvec11(3)*eigvec11(3)
     +       + dfds12*eigvec12(3)*eigvec12(3)
     +       + dfds13*eigvec13(3)*eigvec13(3)
      tmp(4) = dfds11*eigvec11(2)*eigvec11(3)
     +       + dfds12*eigvec12(2)*eigvec12(3)
     +       + dfds13*eigvec13(2)*eigvec13(3)
      tmp(5) = dfds11*eigvec11(1)*eigvec11(3)
     +       + dfds12*eigvec12(1)*eigvec12(3)
     +       + dfds13*eigvec13(1)*eigvec13(3)
      tmp(6) = dfds11*eigvec11(1)*eigvec11(2)
     +       + dfds12*eigvec12(1)*eigvec12(2)
     +       + dfds13*eigvec13(1)*eigvec13(2)
c
c     tmp into the natural notation
      tmpnew(1) = ovsqrt3*(tmp(1)+tmp(2)+tmp(3))
      tmpnew(2) = ovsqrt6*(2.d0*tmp(3)-tmp(1)-tmp(2))
      tmpnew(3) = ovsqrt2*(tmp(2)-tmp(1))
      tmpnew(4) = sqrt2*tmp(4)
      tmpnew(5) = sqrt2*tmp(5)
      tmpnew(6) = sqrt2*tmp(6)
c
c	Compute GRAD
c
      grad(1) = L12*tmpnew(1) + L22*tmpnew(2) + L32*tmpnew(3)
      grad(2) = L13*tmpnew(1) + L23*tmpnew(2) + L33*tmpnew(3)
      grad(3) = L44*tmpnew(4)
      grad(4) = L55*tmpnew(5)
      grad(5) = L66*tmpnew(6)
c	
      tmp(1) = dfds21*eigvec21(1)*eigvec21(1)
     +       + dfds22*eigvec22(1)*eigvec22(1)
     +       + dfds23*eigvec23(1)*eigvec23(1)
      tmp(2) = dfds21*eigvec21(2)*eigvec21(2)
     +       + dfds22*eigvec22(2)*eigvec22(2)
     +       + dfds23*eigvec23(2)*eigvec23(2)
      tmp(3) = dfds21*eigvec21(3)*eigvec21(3)
     +       + dfds22*eigvec22(3)*eigvec22(3)
     +       + dfds23*eigvec23(3)*eigvec23(3)
      tmp(4) = dfds21*eigvec21(2)*eigvec21(3)
     +       + dfds22*eigvec22(2)*eigvec22(3)
     +       + dfds23*eigvec23(2)*eigvec23(3)
      tmp(5) = dfds21*eigvec21(1)*eigvec21(3)
     +       + dfds22*eigvec22(1)*eigvec22(3)
     +       + dfds23*eigvec23(1)*eigvec23(3)
      tmp(6) = dfds21*eigvec21(1)*eigvec21(2)
     +       + dfds22*eigvec22(1)*eigvec22(2)
     +       + dfds23*eigvec23(1)*eigvec23(2)
c
c     tmp into the natural notation
      tmpnew(1) = ovsqrt3*(tmp(1)+tmp(2)+tmp(3))
      tmpnew(2) = ovsqrt6*(2.d0*tmp(3)-tmp(1)-tmp(2))
      tmpnew(3) = ovsqrt2*(tmp(2)-tmp(1))
      tmpnew(4) = sqrt2*tmp(4)
      tmpnew(5) = sqrt2*tmp(5)
      tmpnew(6) = sqrt2*tmp(6)
c
      grad(1) = grad(1) + M12*tmpnew(1) + M22*tmpnew(2) + M32*tmpnew(3)
      grad(2) = grad(2) + M13*tmpnew(1) + M23*tmpnew(2) + M33*tmpnew(3)
      grad(3) = grad(3) + M44*tmpnew(4)
      grad(4) = grad(4) + M55*tmpnew(5)
      grad(5) = grad(5) + M66*tmpnew(6)
c
c     Stop here if Hessian is not required 
c     and only F and GRAD are returned
      if (mode .EQ. 1) return
cc    
c
      as11s21pa2 = as11s21pa1/max(abs(s11s21),tol)
	  as11s22pa2 = as11s22pa1/max(abs(s11s22),tol)
	  as11s23pa2 = as11s23pa1/max(abs(s11s23),tol)
	  as12s21pa2 = as12s21pa1/max(abs(s12s21),tol)
	  as12s22pa2 = as12s22pa1/max(abs(s12s22),tol)
	  as12s23pa2 = as12s23pa1/max(abs(s12s23),tol)
	  as13s21pa2 = as13s21pa1/max(abs(s13s21),tol)
	  as13s22pa2 = as13s22pa1/max(abs(s13s22),tol)
	  as13s23pa2 = as13s23pa1/max(abs(s13s23),tol)
c
      ddfds11ds11 = a1ovf*( ov4*(as11s21pa2 + as11s22pa2 + as11s23pa2)
     +            - dfds11*dfds11 )                                   
      ddfds12ds12 = a1ovf*( ov4*(as12s21pa2 + as12s22pa2 + as12s23pa2)
     +            - dfds12*dfds12 )                                   
      ddfds13ds13 = a1ovf*( ov4*(as13s21pa2 + as13s22pa2 + as13s23pa2)
     +            - dfds13*dfds13 )                                   
      !                                                               
      ddfds21ds21 = a1ovf*( ov4*(as11s21pa2 + as12s21pa2 + as13s21pa2)
     +            - dfds21*dfds21 )                                   
      ddfds22ds22 = a1ovf*( ov4*(as11s22pa2 + as12s22pa2 + as13s22pa2)
     +            - dfds22*dfds22 )                                   
      ddfds23ds23 = a1ovf*( ov4*(as11s23pa2 + as12s23pa2 + as13s23pa2)
     +            - dfds23*dfds23 )
cc
      ddfds11ds12 = -a1ovf*dfds11*dfds12
      ddfds11ds13 = -a1ovf*dfds11*dfds13
      ddfds12ds11 = ddfds11ds12
      ddfds12ds13 = -a1ovf*dfds12*dfds13
      ddfds13ds11 = ddfds11ds13
      ddfds13ds12 = ddfds12ds13
c
      ddfds21ds22 = -a1ovf*dfds21*dfds22
      ddfds21ds23 = -a1ovf*dfds21*dfds23
      ddfds22ds21 = ddfds21ds22
      ddfds22ds23 = -a1ovf*dfds22*dfds23
      ddfds23ds21 = ddfds21ds23
      ddfds23ds22 = ddfds22ds23
cc
      ddfds11ds21 = a1ovf*(mov4*as11s21pa2 - dfds11*dfds21)
      ddfds12ds22 = a1ovf*(mov4*as12s22pa2 - dfds12*dfds22)
      ddfds13ds23 = a1ovf*(mov4*as13s23pa2 - dfds13*dfds23)
      ddfds11ds22 = a1ovf*(mov4*as11s22pa2 - dfds11*dfds22)
      ddfds11ds23 = a1ovf*(mov4*as11s23pa2 - dfds11*dfds23)
      ddfds12ds21 = a1ovf*(mov4*as12s21pa2 - dfds12*dfds21)
      ddfds12ds23 = a1ovf*(mov4*as12s23pa2 - dfds12*dfds23)
      ddfds13ds21 = a1ovf*(mov4*as13s21pa2 - dfds13*dfds21)
      ddfds13ds22 = a1ovf*(mov4*as13s22pa2 - dfds13*dfds22)
c
      ddfds21ds11 = ddfds11ds21
      ddfds21ds12 = ddfds12ds21
      ddfds21ds13 = ddfds13ds21
      ddfds22ds11 = ddfds11ds22
      ddfds22ds12 = ddfds12ds22
      ddfds22ds13 = ddfds13ds22
      ddfds23ds11 = ddfds11ds23
      ddfds23ds12 = ddfds12ds23
      ddfds23ds13 = ddfds13ds23
ccc
      call outer2vec(eigvec11, eigvec11, e11xe11v)
      call outer2vec(eigvec11, eigvec12, e11xe12v)
      call outer2vec(eigvec11, eigvec13, e11xe13v)
      call outer2vec(eigvec12, eigvec11, e12xe11v)
      call outer2vec(eigvec12, eigvec12, e12xe12v)
      call outer2vec(eigvec12, eigvec13, e12xe13v)
      call outer2vec(eigvec13, eigvec11, e13xe11v)
      call outer2vec(eigvec13, eigvec12, e13xe12v)
      call outer2vec(eigvec13, eigvec13, e13xe13v)
c
      call outer2vec(eigvec21, eigvec21, e21xe21v)
      call outer2vec(eigvec21, eigvec22, e21xe22v)
      call outer2vec(eigvec21, eigvec23, e21xe23v)
      call outer2vec(eigvec22, eigvec21, e22xe21v)
      call outer2vec(eigvec22, eigvec22, e22xe22v)
      call outer2vec(eigvec22, eigvec23, e22xe23v)
      call outer2vec(eigvec23, eigvec21, e23xe21v)
      call outer2vec(eigvec23, eigvec22, e23xe22v)
      call outer2vec(eigvec23, eigvec23, e23xe23v)
c    
c
      call symouter6(e11xe11v, e11xe11v, ds11xds11)
      call symouter6(e11xe11v, e12xe12v, ds11xds12)
      call symouter6(e11xe11v, e13xe13v, ds11xds13)
      call symouter6(e12xe12v, e11xe11v, ds12xds11)
	call symouter6(e12xe12v, e12xe12v, ds12xds12)
	call symouter6(e12xe12v, e13xe13v, ds12xds13)
	call symouter6(e13xe13v, e11xe11v, ds13xds11)
	call symouter6(e13xe13v, e12xe12v, ds13xds12)
	call symouter6(e13xe13v, e13xe13v, ds13xds13)
c             					
      call symouter6(e21xe21v, e21xe21v, ds21xds21)
	call symouter6(e21xe21v, e22xe22v, ds21xds22)
	call symouter6(e21xe21v, e23xe23v, ds21xds23)
      call symouter6(e22xe22v, e21xe21v, ds22xds21)
	call symouter6(e22xe22v, e22xe22v, ds22xds22)
	call symouter6(e22xe22v, e23xe23v, ds22xds23)
	call symouter6(e23xe23v, e21xe21v, ds23xds21)
	call symouter6(e23xe23v, e22xe22v, ds23xds22)
	call symouter6(e23xe23v, e23xe23v, ds23xds23)
c           					 		  		   
	call symouter6(e11xe11v, e21xe21v, ds11xds21)
	call symouter6(e11xe11v, e22xe22v, ds11xds22)
	call symouter6(e11xe11v, e23xe23v, ds11xds23)
      call symouter6(e12xe12v, e21xe21v, ds12xds21)
	call symouter6(e12xe12v, e22xe22v, ds12xds22)
	call symouter6(e12xe12v, e23xe23v, ds12xds23)
	call symouter6(e13xe13v, e21xe21v, ds13xds21)
	call symouter6(e13xe13v, e22xe22v, ds13xds22)
	call symouter6(e13xe13v, e23xe23v, ds13xds23)
c             					 		  		   
	call symouter6(e21xe21v, e11xe11v, ds21xds11)
	call symouter6(e21xe21v, e12xe12v, ds21xds12)
	call symouter6(e21xe21v, e13xe13v, ds21xds13)
      call symouter6(e22xe22v, e11xe11v, ds22xds11)
	call symouter6(e22xe22v, e12xe12v, ds22xds12)
	call symouter6(e22xe22v, e13xe13v, ds22xds13)
	call symouter6(e23xe23v, e11xe11v, ds23xds11)
	call symouter6(e23xe23v, e12xe12v, ds23xds12)
	call symouter6(e23xe23v, e13xe13v, ds23xds13)
c
c     note, only upper triangle of 6x6 sym matrices
c     H1 and H2 is calculated. H3 and H4 are not
c     symmetric, but H3 = H4T, so the upper
c     triangle is enough to calculate
	do i=1, 6
		do j=i, 6
              H1(i,j) = ddfds11ds11*ds11xds11(i,j)
     +				+ ddfds11ds12*ds11xds12(i,j)
     +				+ ddfds11ds13*ds11xds13(i,j)
     +				+ ddfds12ds11*ds12xds11(i,j)
     +				+ ddfds12ds12*ds12xds12(i,j)
     +				+ ddfds12ds13*ds12xds13(i,j)
     +				+ ddfds13ds11*ds13xds11(i,j)
     +				+ ddfds13ds12*ds13xds12(i,j)
     +				+ ddfds13ds13*ds13xds13(i,j)
c                                                 
	 		H2(i,j) = ddfds21ds21*ds21xds21(i,j)
     +				+ ddfds21ds22*ds21xds22(i,j)
     +				+ ddfds21ds23*ds21xds23(i,j)
     +				+ ddfds22ds21*ds22xds21(i,j)
     +				+ ddfds22ds22*ds22xds22(i,j)
     +				+ ddfds22ds23*ds22xds23(i,j)
     +				+ ddfds23ds21*ds23xds21(i,j)
     +				+ ddfds23ds22*ds23xds22(i,j)
     +				+ ddfds23ds23*ds23xds23(i,j)
c                                                 
	 		H3(i,j) = ddfds11ds21*ds11xds21(i,j)
     +				+ ddfds11ds22*ds11xds22(i,j)
     +				+ ddfds11ds23*ds11xds23(i,j)
     +				+ ddfds12ds21*ds12xds21(i,j)
     +				+ ddfds12ds22*ds12xds22(i,j)
     +				+ ddfds12ds23*ds12xds23(i,j)
     +				+ ddfds13ds21*ds13xds21(i,j)
     +				+ ddfds13ds22*ds13xds22(i,j)
     +				+ ddfds13ds23*ds13xds23(i,j)
c                                                 
	 		H4(i,j) = ddfds21ds11*ds21xds11(i,j)
     +				+ ddfds21ds12*ds21xds12(i,j)
     +				+ ddfds21ds13*ds21xds13(i,j)
     +				+ ddfds22ds11*ds22xds11(i,j)
     +				+ ddfds22ds12*ds22xds12(i,j)
     +				+ ddfds22ds13*ds22xds13(i,j)
     +				+ ddfds23ds11*ds23xds11(i,j)
     +				+ ddfds23ds12*ds23xds12(i,j)
     +				+ ddfds23ds13*ds23xds13(i,j)
          end do
      end do
c  
c
c     Eq (32) single prime
	call symouter6(e11xe12v, e11xe12v, Etmp1212)
	call symouter6(e11xe12v, e12xe11v, Etmp1221)
	call symouter6(e12xe11v, e11xe12v, Etmp2112)
	call symouter6(e12xe11v, e12xe11v, Etmp2121)
c
      do i=1, 6
          do j=i, 6
	        E1212p(i,j) = Etmp1212(i,j) + Etmp1221(i,j)
     +                    + Etmp2112(i,j) + Etmp2121(i,j)
          end do
      end do
c
      call symouter6(e12xe13v, e12xe13v, Etmp2323)
	call symouter6(e12xe13v, e13xe12v, Etmp2332)
	call symouter6(e13xe12v, e12xe13v, Etmp3223)
	call symouter6(e13xe12v, e13xe12v, Etmp3232)
c
      do i=1, 6
          do j=i, 6
      	    E2323p(i,j) = Etmp2323(i,j) + Etmp2332(i,j)
     +                    + Etmp3223(i,j) + Etmp3232(i,j)
          end do
      end do
c
	call symouter6(e13xe11v, e13xe11v, Etmp3131)
	call symouter6(e13xe11v, e11xe13v, Etmp3113)
	call symouter6(e11xe13v, e13xe11v, Etmp1331)
	call symouter6(e11xe13v, e11xe13v, Etmp1313)
c
      do i=1, 6
          do j=i, 6
	        E3131p(i,j) = Etmp3131(i,j) + Etmp3113(i,j)
     +                    + Etmp1331(i,j) + Etmp1313(i,j)
          end do
      end do
cc
c     Eq (32) double prime
	call symouter6(e21xe22v, e21xe22v, Etmp1212)
	call symouter6(e21xe22v, e22xe21v, Etmp1221)
	call symouter6(e22xe21v, e21xe22v, Etmp2112)
	call symouter6(e22xe21v, e22xe21v, Etmp2121)
c
      do i=1, 6
          do j=i, 6
	        E1212pp(i,j) = Etmp1212(i,j) + Etmp1221(i,j)
     +                     + Etmp2112(i,j) + Etmp2121(i,j)
          end do
      end do
c
      call symouter6(e22xe23v, e22xe23v, Etmp2323)
	call symouter6(e22xe23v, e23xe22v, Etmp2332)
	call symouter6(e23xe22v, e22xe23v, Etmp3223)
	call symouter6(e23xe22v, e23xe22v, Etmp3232)
c
      do i=1, 6
          do j=i, 6
	        E2323pp(i,j) = Etmp2323(i,j) + Etmp2332(i,j)
     +                     + Etmp3223(i,j) + Etmp3232(i,j)
          end do
      end do
c
	call symouter6(e23xe21v, e23xe21v, Etmp3131)
	call symouter6(e23xe21v, e21xe23v, Etmp3113)
	call symouter6(e21xe23v, e23xe21v, Etmp1331)
	call symouter6(e21xe23v, e21xe23v, Etmp1313)
c
      do i=1, 6
          do j=i, 6
	        E3131pp(i,j) = Etmp3131(i,j) + Etmp3113(i,j)
     +                     + Etmp1331(i,j) + Etmp1313(i,j)
          end do
      end do
c
c     single primed
	if (abs(s11-s12) .LT. tol) then
		coefE1212p = ddfds11ds11 - ddfds11ds12
	else
		coefE1212p = (dfds11 - dfds12)/(s11-s12)
	end if
c
	if (abs(s12-s13) .LT. tol) then
		coefE2323p = ddfds12ds12 - ddfds12ds13
	else
		coefE2323p = (dfds12 - dfds13)/(s12-s13)
	end if
c
	if (abs(s13-s11) .LT. tol) then
		coefE3131p = ddfds13ds13 - ddfds13ds11
	else
		coefE3131p = (dfds13 - dfds11)/(s13-s11)
      end if
c
cc    double primed
      if (abs(s21-s22) .LT. tol) then
		coefE1212pp = ddfds21ds21 - ddfds21ds22
	else
		coefE1212pp = (dfds21 - dfds22)/(s21-s22)
	end if
c
	if (abs(s22-s23) .LT. tol) then
		coefE2323pp = ddfds22ds22 - ddfds22ds23
	else
		coefE2323pp = (dfds22 - dfds23)/(s22-s23)
	end if
c
	if (abs(s23-s21) .LT. tol) then
		coefE3131pp = ddfds23ds23 - ddfds23ds21
	else
		coefE3131pp = (dfds23 - dfds21)/(s23-s21)
      end if
c	
c     note, only upper triangle of 6x6 sym matrices
c     H5 and H6 is calculated
	do i=1, 6
		do j=i, 6
			H5(i,j) = (coefE1212p*E1212p(i,j)
     +                +  coefE2323p*E2323p(i,j)         
     +                +  coefE3131p*E3131p(i,j))*ov2/f
c
			H6(i,j) = (coefE1212pp*E1212pp(i,j)
     +                +  coefE2323pp*E2323pp(i,j)       
     +                +  coefE3131pp*E3131pp(i,j))*ov2/f
c
          end do
          end do
c
c     summing up H1 with H5, and H2 with H6, before tarnsforming them
c     together by L and M, respectively
      do i=1, 6
          do j=i, 6
              H7(i,j) = H1(i,j) + H5(i,j)
              H8(i,j) = H2(i,j) + H6(i,j)
          end do
      end do
c
c
c     computing upper part of sym matrix (LT*H3*M + MT*H4*L) (Eq. 25)
c     note that H3 = H4T, then 
c     dsymLTxH3xM = LT*H3*M + (LT*H3*M)T = 2*sym(LT*H3*M)
      tmp1 = H3(1,1)*M12 + H3(1,2)*M22 + H3(1,3)*M32
      tmp2 = H4(1,2)*M12 + H3(2,2)*M22 + H3(2,3)*M32
      tmp3 = H4(1,3)*M12 + H4(2,3)*M22 + H3(3,3)*M32
      dsymLTxH3xM(1,1) =(L12*tmp1 + L22*tmp2 + L32*tmp3)*2.d0
      dsymLTxH3xM(1,2) = L13*tmp1 + L23*tmp2 + L33*tmp3
     +                 + M13*(H3(1,1)*L12 + H4(1,2)*L22 + H4(1,3)*L32)
     +                 + M23*(H3(1,2)*L12 + H3(2,2)*L22 + H4(2,3)*L32)
     +                 + M33*(H3(1,3)*L12 + H3(2,3)*L22 + H3(3,3)*L32)
      dsymLTxH3xM(1,3) = L44*(H4(1,4)*M12 + H4(2,4)*M22 + H4(3,4)*M32)
     +                 + M44*(H3(1,4)*L12 + H3(2,4)*L22 + H3(3,4)*L32)
      dsymLTxH3xM(1,4) = L55*(H4(1,5)*M12 + H4(2,5)*M22 + H4(3,5)*M32)
     +                 + M55*(H3(1,5)*L12 + H3(2,5)*L22 + H3(3,5)*L32)
      dsymLTxH3xM(1,5) = L66*(H4(1,6)*M12 + H4(2,6)*M22 + H4(3,6)*M32)
     +                 + M66*(H3(1,6)*L12 + H3(2,6)*L22 + H3(3,6)*L32)
      dsymLTxH3xM(2,2) =(L13*(H3(1,1)*M13 + H3(1,2)*M23 + H3(1,3)*M33)
     +                 + L23*(H4(1,2)*M13 + H3(2,2)*M23 + H3(2,3)*M33) 
     +                 + L33*(H4(1,3)*M13 + H4(2,3)*M23 + H3(3,3)*M33))
     +                 * 2.d0
      dsymLTxH3xM(2,3) = L44*(H4(1,4)*M13 + H4(2,4)*M23 + H4(3,4)*M33)
     +                 + M44*(H3(1,4)*L13 + H3(2,4)*L23 + H3(3,4)*L33)
      dsymLTxH3xM(2,4) = L55*(H4(1,5)*M13 + H4(2,5)*M23 + H4(3,5)*M33)
     +                 + M55*(H3(1,5)*L13 + H3(2,5)*L23 + H3(3,5)*L33)
      dsymLTxH3xM(2,5) = L66*(H4(1,6)*M13 + H4(2,6)*M23 + H4(3,6)*M33)
     +                 + M66*(H3(1,6)*L13 + H3(2,6)*L23 + H3(3,6)*L33)
      dsymLTxH3xM(3,3) = 2.d0*H3(4,4)*L44*M44
      dsymLTxH3xM(3,4) = H3(4,5)*L44*M55 + H4(4,5)*L55*M44
      dsymLTxH3xM(3,5) = H3(4,6)*L44*M66 + H4(4,6)*L66*M44
      dsymLTxH3xM(4,4) = 2.d0*H3(5,5)*L55*M55
      dsymLTxH3xM(4,5) = H3(5,6)*L55*M66 + H4(5,6)*L66*M55
      dsymLTxH3xM(5,5) = 2.d0*H3(6,6)*L66*M66
c
c     copmuting upper part of sym matrix LT*H7*L = LT*(H1+H5)*L (Eq. 25)
      LTxH7xL(1,1) = H7(1,1)*L12**2 + H7(2,2)*L22**2 + H7(3,3)*L32**2  
     +    + (H7(2,3)*L22*L32 + H7(1,3)*L12*L32 + H7(1,2)*L12*L22)*2.d0
      LTxH7xL(1,2) = L13*(H7(1,1)*L12 + H7(1,2)*L22 + H7(1,3)*L32)     
     +             + L23*(H7(1,2)*L12 + H7(2,2)*L22 + H7(2,3)*L32)     
     +             + L33*(H7(1,3)*L12 + H7(2,3)*L22 + H7(3,3)*L32)
      LTxH7xL(1,3) = L44*(H7(1,4)*L12 + H7(2,4)*L22 + H7(3,4)*L32)
      LTxH7xL(1,4) = L55*(H7(1,5)*L12 + H7(2,5)*L22 + H7(3,5)*L32)
      LTxH7xL(1,5) = L66*(H7(1,6)*L12 + H7(2,6)*L22 + H7(3,6)*L32)
      LTxH7xL(2,2) = H7(1,1)*L13**2 + H7(2,2)*L23**2 + H7(3,3)*L33**2  
     +    + (H7(2,3)*L23*L33 + H7(1,3)*L13*L33 + H7(1,2)*L13*L23)*2.d0
      LTxH7xL(2,3) = L44*(H7(1,4)*L13 + H7(2,4)*L23 + H7(3,4)*L33)
      LTxH7xL(2,4) = L55*(H7(1,5)*L13 + H7(2,5)*L23 + H7(3,5)*L33)
      LTxH7xL(2,5) = L66*(H7(1,6)*L13 + H7(2,6)*L23 + H7(3,6)*L33)
      LTxH7xL(3,3) = H7(4,4)*L44**2
      LTxH7xL(3,4) = H7(4,5)*L44*L55
      LTxH7xL(3,5) = H7(4,6)*L44*L66
      LTxH7xL(4,4) = H7(5,5)*L55**2
      LTxH7xL(4,5) = H7(5,6)*L55*L66
      LTxH7xL(5,5) = H7(6,6)*L66**2
c
c     copmuting upper part of sym matrix MT*H8*M = MT*(H2+H6)*M (Eq. 25)
      MTxH8xM(1,1) = H8(1,1)*M12**2 + H8(2,2)*M22**2 + H8(3,3)*M32**2  
     +    + (H8(2,3)*M22*M32 + H8(1,3)*M12*M32 + H8(1,2)*M12*M22)*2.d0
      MTxH8xM(1,2) = M13*(H8(1,1)*M12 + H8(1,2)*M22 + H8(1,3)*M32)     
     +             + M23*(H8(1,2)*M12 + H8(2,2)*M22 + H8(2,3)*M32)     
     +             + M33*(H8(1,3)*M12 + H8(2,3)*M22 + H8(3,3)*M32)
      MTxH8xM(1,3) = M44*(H8(1,4)*M12 + H8(2,4)*M22 + H8(3,4)*M32)
      MTxH8xM(1,4) = M55*(H8(1,5)*M12 + H8(2,5)*M22 + H8(3,5)*M32)
      MTxH8xM(1,5) = M66*(H8(1,6)*M12 + H8(2,6)*M22 + H8(3,6)*M32)
      MTxH8xM(2,2) = H8(1,1)*M13**2 + H8(2,2)*M23**2 + H8(3,3)*M33**2  
     +    + (H8(2,3)*M23*M33 + H8(1,3)*M13*M33 + H8(1,2)*M13*M23)*2.d0
      MTxH8xM(2,3) = M44*(H8(1,4)*M13 + H8(2,4)*M23 + H8(3,4)*M33)
      MTxH8xM(2,4) = M55*(H8(1,5)*M13 + H8(2,5)*M23 + H8(3,5)*M33)
      MTxH8xM(2,5) = M66*(H8(1,6)*M13 + H8(2,6)*M23 + H8(3,6)*M33)
      MTxH8xM(3,3) = H8(4,4)*M44**2
      MTxH8xM(3,4) = H8(4,5)*M44*M55
      MTxH8xM(3,5) = H8(4,6)*M44*M66
      MTxH8xM(4,4) = H8(5,5)*M55**2
      MTxH8xM(4,5) = H8(5,6)*M55*M66
      MTxH8xM(5,5) = H8(6,6)*M66**2
c
c     suming up the final hessian - due to the major symmetry of Hessian
c      only the upper triangle is calculated
      do i=1, 5
          do j=i, 5
              hessian(i,j) = LTxH7xL(i,j) + MTxH8xM(i,j) 
     +                     + dsymLTxH3xM(i,j)
          end do
      end do
c
      return
      end subroutine Yld2004
c
c
      subroutine chol_decomp(A, n, choldiag)
c**********************************************************************
c      The CHOL_DECOMP function calculates the Cholesky decomposition
c      A = L*LT of matrix A, where L is lower-triangular matrix and is
c     stored in the lower triangle of A except for the diagonal, which
c     is stored in array CHOLDIAG
c
c**********************************************************************
c (IN/OUT) A is REAL*8 array, dimension (n,n)
c          Matrix A must be a positive-definite symmetric matrix and
c          at input it is read from the upper triangle of A. At return,
c          the factorized matrix L will be stored in the lower triangle
c          of A.
c (IN)     N is INTEGER,
c          It is the dimension of A.
c (OUT)    CHOLDIAG is REAL*8 array, dimension (n)
c          It contains diagonal Cholesky factors of matrix A. 
c
      implicit none
      integer     i, j, k, n
      real*8      A(n,n), choldiag(n), sum
c
c     Cholesky factorization of A
      do i=1, n
         do j=i, n
            sum = A(i,j)
            do k=i-1, 1, -1
               sum = sum - A(i,k)*A(j,k)
            end do
            if (i .EQ. j) then
               choldiag(i) = sqrt(sum)
            else
               A(j,i) = sum/choldiag(i)
            end if
         end do
      end do
c 
      return
      end subroutine chol_decomp
c
c
      subroutine chol_solve(A, n, b, choldiag, x)
c**********************************************************************
c      The CHOL_SOLVE function returns an n-element vector X containing 
c      the solution to the set of linear equations Ax = b. The Cholesky
c      factorization of A must be stored in the lower-diagonal of A and
c      the diagonal factors in CHOLDIAG.
c
c**********************************************************************
c (IN)     A is REAL*8 array, dimension (n,n)
c          Array A contains the original matrix A stored in the upper 
c          triangle including the diagonal. The Cholesky factorization
c          of A must at input be stored in the lower triangle, and
c          the diagonal factors stored in CHOLDIAG.
c (IN)     N is INTEGER,
c          It is the dimension of the linear system Ax = b
c (IN)     B is REAL*8 array, dimension (n)
c          It is the right-hand-side vector of the linear system Ax = b
c (IN)     CHOLDIAG is REAL*8 array, dimension (n)
c          It contains diagonal Cholesky factors of matrix A.
c (OUT)    X is REAL*8, dimension (n)
c          It is the solution of the linear system Ax = b
c
      implicit none
      integer     i, j, k, n
      real*8      A(n,n), b(n), choldiag(n), x(n), sum
c
c     backsubstitution
      do i=1, n
         sum = b(i)
         do k=i-1, 1, -1
            sum = sum - A(i,k)*x(k)
         end do
         x(i) = sum/choldiag(i)
      end do
c
      do i=n, 1, -1
         sum = x(i)
         do k=i+1, n
            sum = sum - A(k,i)*x(k)
         end do
         x(i) = sum/choldiag(i)
      end do

      return
      end subroutine chol_solve
c
c
      subroutine chol_inverse(A, n, choldiag, C)
c**********************************************************************
c      The CHOL_INVERSE function returns the matrix C which is inverse 
c      of a positive-definite symmetric matrix A.
c
c**********************************************************************
c (IN)     A is REAL*8 array, dimension (n,n)
c          Array A contains the original matrix A stored in the upper 
c          triangle including the diagonal. The Cholesky factorization
c          of A must at input be stored in the lower triangle, and
c          the diagonal factors stored in CHOLDIAG.
c (IN)     N is INTEGER,
c          It is the dimension of the linear system Ax = b
c (IN)     B is REAL*8 array, dimension (n)
c          It is the right-hand-side vector of the linear system Ax = b
c (IN)     CHOLDIAG is REAL*8 array, dimension (n)
c          It contains diagonal Cholesky factors of matrix A.
c (OUT)    C is REAL*8, dimension (n,n)
c          It contains inverse of A
c
      implicit none
      integer     i, j, k, n, m
      real*8      A(n,n), b(n), choldiag(n), sum, C(n,n)
c
c     inverse of A by factorized A stored in lower
c     triangle of A and in CHOLDIAG
      do m=1, n
c         create the cartesian basis vectors b    
          do i=1, n
              if (m .EQ. i) then
                  b(i) = 1.d0
              else
                  b(i) = 0.d0
              end if
          end do
c         fill the columns of C with the solutions x=A-1*b
          do i=1, n
             sum = b(i)
             do k=i-1, 1, -1
                sum = sum - A(i,k)*C(k,m)
             end do
             C(i,m) = sum/choldiag(i)
          end do
c
          do i=n, 1, -1
             sum = C(i,m)
             do k=i+1, n
                sum = sum - A(k,i)*C(k,m)
             end do
             C(i,m) = sum/choldiag(i)
          end do
      end do
c      
      return
      end subroutine chol_inverse
c
c
      subroutine outer2vec(e1, e2, v)
c**********************************************************************
c     Computes the outer product of E1 and E2 vector if dimension 3 and 
c     stores it in vector V in the natural notation
c**********************************************************************
	implicit none
	integer     i, j
	real*8      e1(3), e2(3), v(6), tmp(6)
      real*8      ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
      common /consts/ ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
c	
	tmp(1) = e1(1)*e2(1)
	tmp(2) = e1(2)*e2(2)
	tmp(3) = e1(3)*e2(3)
	tmp(4) = e1(2)*e2(3)
	tmp(5) = e1(1)*e2(3)
	tmp(6) = e1(1)*e2(2)
c	
	v(1) = ovsqrt3*(tmp(1)+tmp(2)+tmp(3))
	v(2) = ovsqrt6*(2.d0*tmp(3)-tmp(1)-tmp(2))
	v(3) = ovsqrt2*(tmp(2)-tmp(1))
	v(4) = sqrt2*tmp(4)
	v(5) = sqrt2*tmp(5)
	v(6) = sqrt2*tmp(6)
c
      return
      end subroutine outer2vec
c
c
      subroutine symouter6(e1, e2, M)
c**********************************************************************
c     Computes the upper triangle of the outer product of E1 and E2 
c     vectors of dimension 6 given in the natural notation and stores
c     it in 6x6 matrix M
c**********************************************************************
	implicit none
	integer     i, j
	real*8      e1(6), e2(6), M(6,6)
c		
      do i=1, 6
		do j=i, 6
			M(i,j) = e1(i)*e2(j)
		end do
      end do
c
      return
      end subroutine symouter6
c
c
      real*8 function vdot(x, y, n)
c**********************************************************************
c     Function, that computes the dot product of vectors X and Y of 
c     dimension N
c**********************************************************************
c          
      implicit none
      integer     n, i
      real*8      x(n), y(n)
c          
      vdot = 0.d0
      do i = 1, n
          vdot = vdot + x(i)*y(i)
      end do
c          
      return
      end function vdot
