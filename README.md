# Continuum plasticity

![matplotlib screencast](anim_Yld2004.gif)

## Contributors
Tomas Manik (researcher), Bjørn Holmedal (Professor) in the Physical Metallurgy Group at the Department of Materials Science and Engineering, Norwegian University of Technology, Trondheim, Norway. 

https://www.ntnu.edu/ima/research/physical-metallurgy#/view/about

## About

The advanced anisotropic yield functions dedicated for modelling of medium and strong plastic anisotropy in metals have been existing longer than a decade. However, their full exploitation has been hindered by the lack of a fast and robust implementation. As a consequence, the most widely used models in sheet metal forming are either isotropic Von Mises or the Hill yield surface. Recently [1], a robust implicit backward Euler return-mapping algorithm was proposed and applied successfully to Barlat’s Yld2004 [2] yield function. The algorithm was further improved and implemented in a user-defined material subroutine (UMAT) for Abaqus FE software. Our results [3] showed that the new algorithm is equally fast and robust as the simple Von Mises and Hill standard implementation. This enables the full exploitation of advanced yield functions in industrial FE applications and sets the new standard for the metal forming industry. 

[1] Scherzinger, W.M., 2017. A return mapping algorithm for isotropic and anisotropic plasticity models using a line search method. Computer Methods in Applied Mechanics and Engineering 317, 526-553. DOI https://doi.org/10.1016/j.cma.2016.11.026

[2] Barlat, F., Aretz, H., Yoon, J.W., Karabin, M.E., Brem, J.C., Dick, R.E., 2005. Linear transfomation-based anisotropic yield functions. International Journal of Plasticity 21, 1009-1039. DOI https://doi.org/10.1016/j.ijplas.2004.06.004

[3] Manik, T., 2021. A natural vector/matrix notation applied in an efficient and robust return-mapping algorithm for advanced yield functions, European Journal of Mechanics - A/Solids, Volume 90, DOI https://doi.org/10.1016/j.euromechsol.2021.104357

## Example
The Abaqus input file `tension.inp` contains a FE model of an rectangular tensile specimen subjected to an uniaxial tension, discussed in [3]. Using the provided UMAT, you can run it as

```
abaqus job=tension user=umatYLD2004.for
```

## Contact
Tomas Manik - tomas.manik@ntnu.no

Bjørn Holmedal - bjorn.holmedal@ntnu.no 
